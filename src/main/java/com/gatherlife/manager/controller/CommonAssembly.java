package com.gatherlife.manager.controller;

import org.xson.common.object.XCO;
import org.xson.tangyuan.web.RequestContext;

public class CommonAssembly {

	public void addUserInfo(RequestContext requestContext) {
		XCO arg = (XCO) requestContext.getArg();
		if (null == arg) {
			arg = new XCO();
			requestContext.setArg(arg);
		}
		XCO user = (XCO) requestContext.getAttach().get("PLATFORM_USER");
		arg.setXCOValue("tokenUser", user);
	}
	
	public void log(RequestContext requestContext){
		XCO arg = (XCO) requestContext.getArg();
		if(null == arg){
			System.out.println(requestContext.getUrl() + "::::arg=" + null);
		}else{
			System.out.println(requestContext.getUrl() + "::::arg=" + arg);
		}
	}
}
