package com.gatherlife.manager.utils;

import java.util.ArrayList;
import java.util.List;

public class NavTree {
	public long				menu_id;
	public String			menu_name;
	public String			menu_icon;
	public String			menu_url;
	public int				menu_level;
	public long				menu_fid;
	public int				menu_sort;
	public int				menu_type;
	public int				menu_state;
	public List<NavTree>	children	= new ArrayList<NavTree>();
}
